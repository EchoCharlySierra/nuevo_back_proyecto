FROM node

# Dentro de esa imagen node, vamos al apiecs

WORKDIR /apiecs

# Copia el contenido del directorio en el contenedor, en /apiecs
ADD . /apiecs

# Ahora necesitamos indicarle el puerto, el 3000
EXPOSE 3000

#En el directorio de trabajo necesitamos un comando para que la API empiece a funcionar
CMD ["node", "server.js"]
