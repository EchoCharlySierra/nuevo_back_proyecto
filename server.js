// Global variables
global.config = require('./config');

var express = require('express');
// Incluimos JSON Web Token, para autenticarnos con el servidor mediante Tokens de una forma simple y segura.
var jwt = require('jsonwebtoken')

var app = express();
var port = process.env.PORT || global.config.db.port;
// vamos a usar el framework

// para poder tratar el body
var bodyParser = require ('body-parser');
app.use(bodyParser.json());

// para el consumo de la parte de la API de MLAB, ponemos los trozos básicos de URL que habrá que repetir
var baseMLabURL = global.config.db.baseURL;
var mLabAPIKey = global.config.db.apiKey;
var requestJson = require('request-json');

app.listen(port); // estamos abriendo un canal lógico
console.log("API para The Count Accounts escuchando en el puerto " + port);




// VERSION 2 de Login, en la que hacemos login  buscando y actualizando sobre de MongoDB,
// lo que obtenemos mediante la API a MLAB
// Hacemos un put, no un post. Post es para crear y Put para modificar
app.put("/apiecs/v2/login",
  function(req,res) {
              console.log("PUT  /apiecs/v2/login");


          var loginUser = {
            "email" : req.body.email,
            "password" : req.body.password
          }

          // Para el caso de que lo encuentre el usuario, para poder ponerle el logged= true
          var putBody = '{"$set":{"logged":true}}';

          // esto es lo que se usa para hacer una query en la API de Mlab
          var query = 'q={"email":"' + loginUser.email + '", "password":"' + loginUser.password +'"}'

          var httpClient = requestJson.createClient(baseMLabURL);

          var response = {};

          // Para implementar el JWT
          var tokenData;
          var token;

          // A partir de aqui se hacen las peticion para buscar ese usuario y clave, incluyendo el substring de la query y el &:
          httpClient.get("user?" + query + "&" + mLabAPIKey,
            // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
            // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
            function(err, resMLab, body){

              if (err){
                 response =  {
                  "msg" : "Error obteniendo usuario"
                }
                res.status(500);
                res.send(response);
              } else {
                if (body.length > 0){
                  console.log("Encontrado el usuario " + loginUser.email);
                  //Vamos a escribir el campo logged=true en la base de datos
                  httpClient.put("user?" + query + "&" + mLabAPIKey , JSON.parse(putBody),
                    function (errEscritura,resMLabEscritura, bodyEscritura){
                      if (err){
                        res.status(505);
                        response =  {
                         "msg" : "Error escribiendo el logged=true"
                          }
                      } else {
                        if (bodyEscritura.lenght >0) {
                          res.status(200);
                          response = body[0];  // Devolvemos el usuario que hemos encontrado
                        }else{
                          res.status(200);
                          response = body[0];   // Devolvemos el usuario que hemos encontrado
                        }
                        //  Como nos hemos conectado correctamente, vamos a generar
                        // un token que pasaremos al cliente para que lo
                        // guarde y, cada vez que quiera invocar a un método de
                        // esta API, tendrá que pasar ese Token.
                        tokenData = {
                           username: loginUser.email
                         }

                         token = jwt.sign(tokenData, 'Password secreta', {
                            expiresIn: 60 * 60 * 24 // expires in 24 hours
                         });
                         response.token = token;  // Añadinos el token a la respuesta
                      }
                      res.send(response);     // Hacemos el res.send al final de la función manejadora del put
                    }
                  );    // Fin del put
                } else {              // El body de la petición de get es vacío, no se ha encontrado usuario
                   response = {
                     "msg" : "Usuario no encontrado"
                   };
                   res.status(404);
                   res.send(response);
                }
              }
            }
          ) ;    // Fin del get

  }

);    // Fin del V2 login


// VERSION 2 de logout, en la que hacemos logout pero buscando y actualizando sobre lo de MongoDB, lo que obtenemos
// mediante la API a MLAB
// Hacemos un put, no un post. Post es para crear y Put para modificar
app.put("/apiecs/v2/logout",
  function(req,res) {
              console.log("PUT  /apiecs/v2/logout");


          var logoutUser = {
            "id" : req.body.id
          }


          // Vamos a comprobar si el el cliente que ha invocado a este método pasa
          // un webtoken correcto. Si no, no devuelvo datos.
          var token = req.headers['authorization']

           if(!token){
               res.status(401).send({
                 error: "Es necesario el token de autenticación"
               })
               return
           }

           token = token.replace('Bearer ', '')

           jwt.verify(token, 'Password secreta', function(err, user) {
             if (err) {
               res.status(401).send({
                     error: 'Token inválido'
                   });
             } else {
                  console.log("Token válido");
             }
           });

           // Si llegamos aquí es porque se ha verificado el token correctamente.
           // Si no, ya se habría hecho un send del error.


          // Para el caso de que lo encuentre el usuario, para poder borrar la propiedad logged
          var putBody = '{"$unset":{"logged":""}}';

          // esto es lo que se usa para hacer una query en la API de Mlab
          var query = 'q={"id": ' + logoutUser.id +'}'

          var httpClient = requestJson.createClient(baseMLabURL);

          var response = {};


          // A partir de aqui se hacen las peticion para buscar ese usuario y clave, incluyendo el substring de la query y el &:
          httpClient.get("user?" + query + "&" + mLabAPIKey,
            // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
            // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
            function(err, resMLab, body){

              if (err){
                 res.status(500);
                 response =  {
                  "msg" : "Error obteniendo usuario"
                 }
                 res.send(response);
              } else {
                if (body.length > 0){
                  console.log("Encontrado el usuario " + logoutUser.id);
                  //Vamos a borrar el campo logged en la base de datos
                  httpClient.put("user?" + query + "&" + mLabAPIKey , JSON.parse(putBody),
                    function (errEscritura,resMLabEscritura, bodyEscritura){
                      if (err){
                        response =  {
                         "msg" : "Error borrando el logged"
                          }
                        res.status(505);
                      } else {
                        if (bodyEscritura.lenght >0) {
                          res.status(200);
                          response = body[0];     // Devolvemos el usuario que hemos encontrado
                        } else {
                          res.status(200);
                          response = body[0];     // Devolvemos el usuario que hemos encontrado

                        }
                      }
                      res.send(response);   // Hacemos el res.send al final de la función manejadora del put
                    }
                  );   // Final del put

                } else {              // El body de la petición de get es vacío, no se ha encontrado usuario
                   response = {
                     "msg" : "Usuario no encontrado"
                   };
                   res.status(404);
                   res.send(response);
                }
              }
            }
          )     // Fin del get

  }

);    // Fin del V2 logout



// Query para obtener un usuario por id
// Este método no lo vamos a invocar, sino que lo haremos con /v2/user y pasando
// el id en el header porque, por lo visto, con Openshift a veces da problemas
// el incluir una propiedad en medio de la url.
// Así todo, dejamos este método en la API, por si se quisiera usar en algún momento.
app.get("/apiecs/v2/users/:id",
  function (req,res){
    console.log("GET /apiecs/v2/users/:id");

    // recojo el id que es un parámetro
    var id = req.params.id;
    // Vamos a comprobar si el el cliente que ha invocado a este método pasa
    // un webtoken correcto. Si no, no devuelvo datos.
    var token = req.headers['authorization']

     if(!token){
         res.status(401).send({
           error: "Es necesario el token de autenticación"
         })
         return
     }

     token = token.replace('Bearer ', '')

     jwt.verify(token, 'Password secreta', function(err, user) {
       if (err) {
         res.status(401).send({
               error: 'Token inválido'
             });
       } else {
            console.log("Token válido");
       }
     });

     // Si llegamos aquí es porque se ha verificado el token correctamente.
     // Si no, ya se habría hecho un send del error.

    // esto es lo que se usa para hacer una query en la API de Mlab
    var query = 'q={"id":' + id + '}'

    var httpClient = requestJson.createClient(baseMLabURL);

    // A partir de aqui se hacen las peticiones para la query, incluyendo el substring de la query y el &:
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
      // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
      function(err, resMLab, body){
        // En vez de hacer lo que está comentado, vamos a tratar el estatus.
        //  var response = !err ? body : {            // Si no hay error entonces devuelvo body, si no,  mensaje
        //    "msg" : "Error obteniendo usuario."
        //  }

        var response = {};

        if (err){
           response =  {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0){
            response = body;
          } else {
             response = {
               "msg" : "Usuario no encontrado"
             };
             res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
);    // Fin users/{{id}}



// Query para obtener un usuario por id, pasándolo en el headers
app.get("/apiecs/v2/user",
  function (req,res){
    console.log("GET /apiecs/v2/user");

    // recojo el id que esté en el body
    var id = req.headers.id;

    // Vamos a comprobar si el el cliente que ha invocado a este método pasa
    // un webtoken correcto. Si no, no devuelvo datos.
    var token = req.headers['authorization']

     if(!token){
         res.status(401).send({
           error: "Es necesario el token de autenticación"
         })
         return
     }

     token = token.replace('Bearer ', '')

     jwt.verify(token, 'Password secreta', function(err, user) {
       if (err) {
         res.status(401).send({
               error: 'Token inválido'
             });
       } else {
            console.log("Token válido");
       }
     });

     // Si llegamos aquí es porque se ha verificado el token correctamente.
     // Si no, ya se habría hecho un send del error.

    // esto es lo que se usa para hacer una query en la API de Mlab
    var query = 'q={"id":' + id + '}'

    var httpClient = requestJson.createClient(baseMLabURL);

    // A partir de aqui se hacen las peticiones para la query, incluyendo el substring de la query y el &:
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
      // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
      function(err, resMLab, body){
        // En vez de hacer lo que está comentado, vamos a tratar el estatus.
        //  var response = !err ? body : {            // Si no hay error entonces devuelvo body, si no,  mensaje
        //    "msg" : "Error obteniendo usuario."
        //  }

        var response = {};

        if (err){
           response =  {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0){
            response = body;
          } else {
             response = {
               "msg" : "Usuario no encontrado"
             };
             res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
);   // fin get   /apiecs/v2/user



// Query para obtener las cuentas de un usuario por id
// Este método no lo vamos a invocar, sino que lo haremos con /v2/users/accounts y pasando
// el id usuario en el header porque, por lo visto, con Openshift a veces da problemas
// el incluir una propiedad en medio de la url.
// Así todo, dejamos este método en la API, por si se quisiera usar en algún momento.

app.get("/apiecs/v2/users/:id/accounts",
  function (req,res){
    console.log("GET /apiecs/v2/users/:id/accounts");

    // recojo el id que es un parámetro
    var id = req.params.id;

    // Vamos a comprobar si el el cliente que ha invocado a este método pasa
    // un webtoken correcto. Si no, no devuelvo datos.
    var token = req.headers['authorization']

     if(!token){
         res.status(401).send({
           error: "Es necesario el token de autenticación"
         })
         return
     }

     token = token.replace('Bearer ', '')

     jwt.verify(token, 'Password secreta', function(err, user) {
       if (err) {
         res.status(401).send({
               error: 'Token inválido'
             });
       } else {
            console.log("Token válido");
       }
     });

     // Si llegamos aquí es porque se ha verificado el token correctamente.
     // Si no, ya se habría hecho un send del error.


    // esto es lo que se usa para hacer una query en la API de Mlab
    var query = 'q={"user_id":' + id + '}'

    var httpClient = requestJson.createClient(baseMLabURL);

    // A partir de aqui se hacen las peticiones para la query, buscamos en cuentas, incluyendo el substring de la query y el &:
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
      // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
      function(err, resMLab, body){
        // En vez de hacer lo que está comentado, vamos a tratar el estatus.
        //  var response = !err ? body : {            // Si no hay error entonces devuelvo body, si no,  mensaje
        //    "msg" : "Error obteniendo usuario."
        //  }

        var response = {};

        if (err){
           response =  {
            "msg" : "Error obteniendo cuentas de usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0){
            response = body;
          } else {
             response = {
               "msg" : "Usuario no encontrado"
             };
             res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
);      // Fin del get de cuentas de un usuario /apiecs/v2/users/:id/accounts


// Query para obtener las cuentas de un usuario por id, que le hemos pasado
// en los headers. No lo pasamos como parámetro para evitar posibles problemas
// con OpenShift (ver método anterior)
app.get("/apiecs/v2/user/accounts",
  function (req,res){
    console.log("GET /apiecs/v2/user/accounts");

    // recojo el id del usuario que lo hemos pasado en los headers
    var id = req.headers.id;
    // Vamos a comprobar si el el cliente que ha invocado a este método pasa
    // un webtoken correcto. Si no, no devuelvo datos.
    var token = req.headers['authorization']

     if(!token){
         res.status(401).send({
           error: "Es necesario el token de autenticación"
         })
         return
     }

     token = token.replace('Bearer ', '')

     jwt.verify(token, 'Password secreta', function(err, user) {
       if (err) {
         res.status(401).send({
               error: 'Token inválido'
             });
       } else {
            console.log("Token válido");
       }
     });

     // Si llegamos aquí es porque se ha verificado el token correctamente.
     // Si no, ya se habría hecho un send del error.


    // esto es lo que se usa para hacer una query en la API de Mlab
    var query = 'q={"user_id":' + id + '}'
    var httpClient = requestJson.createClient(baseMLabURL);

    // A partir de aqui se hacen las peticiones para la query, buscamos en cuentas, incluyendo el substring de la query y el &:
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
      // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
      function(err, resMLab, body){
        // En vez de hacer lo que está comentado, vamos a tratar el estatus.
        //  var response = !err ? body : {            // Si no hay error entonces devuelvo body, si no,  mensaje
        //    "msg" : "Error obteniendo usuario."
        //  }

        var response = {};

        if (err){
           response =  {
            "msg" : "Error obteniendo cuentas de usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0){
            response = body;
          } else {
             response = {
               "msg" : "Usuario no encontrado"
             };
             res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
);      // Fin del get de cuentas de un usuario /apiecs/v2/user/accounts




// VERSION 2 de modificar usuario,  actualizando sobre lo de MongoDB, lo que obtenemos
// mediante la API a MLAB
// Hacemos un put, no un post. Post es para crear y Put para modificar
app.put("/apiecs/v2/update_user",
  function(req,res) {
              console.log("PUT  /apiecs/v2/update_user");


          var user = {
            "id" : req.body.id,
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "city" : req.body.city,
            "email" : req.body.email,
            "password" : req.body.password
          }

          // Vamos a comprobar si el el cliente que ha invocado a este método pasa
          // un webtoken correcto. Si no, no devuelvo datos.
          var token = req.headers['authorization']

           if(!token){
               res.status(401).send({
                 error: "Es necesario el token de autenticación"
               })
               return
           }

           token = token.replace('Bearer ', '')

           jwt.verify(token, 'Password secreta', function(err, user) {
             if (err) {
               res.status(401).send({
                     error: 'Token inválido'
                   });
             } else {
                  console.log("Token válido");
             }
           });

           // Si llegamos aquí es porque se ha verificado el token correctamente.
           // Si no, ya se habría hecho un send del error.


          // Para el caso de que lo encuentre el usuario, para poder ponerle
          // los nuevos valores

          var putBody = '{"$set":{"first_name":"' + user.first_name + '",' +
                      '"last_name":"' + user.last_name + '",' +
                      '"city":"' + user.city + '",' +
                      '"password":"' + user.password + '"}}';

          // esto es lo que se usa para hacer una query en la API de Mlab
          var query = 'q={"email":"' + user.email + '"}'

          var httpClient = requestJson.createClient(baseMLabURL);

          var response = {};

                  //Vamos a escribir los campos modificados en la base de datos
                  httpClient.put("user?" + query + "&" + mLabAPIKey , JSON.parse(putBody),
                    function (errEscritura,resMLabEscritura, bodyEscritura){
                      if (errEscritura){
                        res.status(505);
                        response =  {
                         "msg" : "Error actualizando el usuario"
                          }
                      } else {
                        if (bodyEscritura.lenght >0) {
                          res.status(200);
                          response = user;  // Devolvemos el usuario que hemos modificado
                        }else{
                          res.status(200);
                          response = user;   // Devolvemos el usuario que hemos modificado
                        }
                      }
                      res.send(response);     // Hacemos el res.send al final de la función manejadora del put
                    }
                  );    // Fin del put
  }

);    // Fin del V2 modificar usuario


// VERSION 2 de Registrar (dar de alta) un usuario,  buscando si ya estuviera en MongoDB, lo que obtenemos
// mediante la API a MLAB
// Hacemos un post, no un put. Post es para crear y Put para modificar
app.post("/apiecs/v2/register",
  function(req,res) {
              console.log("POST  /apiecs/v2/register");


          var registerUser = {
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "city" : req.body.city,
            "email" : req.body.email,
            "password" : req.body.password,
            "id" : 0      // En principioa cero, ya lo rellenaremos después
                      // con el máximo que haya en la bbdd + 1
          }


          // Para el caso de que lo encuentre el usuario, para poder ponerle el logged= true
//          var putBody = '{"$set":{"logged":true}}';

          // esto es lo que se usa para hacer una query en la API de Mlab,
          // para ver si el usuario (su email) ya existía.
          var query = 'q={"email":"' + registerUser.email  +'"}'

          var httpClient = requestJson.createClient(baseMLabURL);

          var response = {};

          // En esta variable meteremos el valor máximo del id encontrado,
          // para insertar ese máximo + 1 en el nuevo usuario que se cree
          var maxid = 0;

          // A partir de aqui se hacen las peticion para buscar ese usuario,
          // incluyendo el substring de la query y el &:
          httpClient.get("user?" + query + "&" + mLabAPIKey,
            // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
            // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
            function(err, resMLab, body){

              if (err){
                 response =  {
                  "msg" : "Error buscando email"
                }
                res.status(500);
                res.send(response);
                console.log("Error al buscar email ya en la BBDD");
                console.log (err);
              } else {
                if (body.length > 0){
                  console.log("Ya hay un usuario con email: " + registerUser.email);
                  response = {
                    "msg" : "Ya existe ese email en la base de datos"
                  };
                  res.status(500);
                  res.send(response);

                } else {     // El body de la petición de get es vacío,
                            //no se ha encontrado el email, entonces, damos de
                            //alta uno

                      // Primero vamos a buscar el valor máximo del id, para
                      // crear el nuevo con el número siguiente.
                      // se hace una query ern la que sólo se ordena
                      // por id descendente y se coge un único registro

                      // sólo traemos el id, ordenamos descendente y cogemos
                      // el primer registro, así sacamos el máximo
                      var querymaxid = 'f={"id": 1}&s={"id":-1}&l=1';

                      var responsemaxid = {};

                      // A partir de aqui se hacen las peticion para buscar
                      // el máximo del id,
                      // incluyendo el substring de la query y el &:
                      httpClient.get("user?" + querymaxid + "&" + mLabAPIKey,
                        // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
                        // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
                        function(errmaxid, resMLabmaxid, bodymaxid){

                          if (errmaxid){
                             responsemaxid =  {
                              "msg" : "Error buscando el máximo id"
                            }
                            res.status(500);
                            res.send(responsemaxid);
                            console.log("Error al buscar el máximo id");
                            console.log (err);
                          } else {
                            if (bodymaxid.length > 0){
                              console.log("Ya tenemos el registro con máximo id: ");
                              maxid = bodymaxid[0].id ;

                            } else {     // El body de la busqueda de max id es vacío,
                              //no se ha encontrado ningún registro, entonces
                              // lo ponemos a 0, para que se meta el nuevo id =1
                                  maxid = 0;
                            }
                          }

                        // Ahora que ya tenemos el máximo id actual de la bbdd,
                        // vamos a rellenar el único campo que nos quedaba

                        registerUser.id = maxid + 1;

                        httpClient.post("user?" + mLabAPIKey , registerUser,
                        function (errEscritura,resMLabEscritura, bodyEscritura){
                          if (errEscritura){
                            res.status(505);
                            response =  {
                             "msg" : "Error escribiendo el nuevo registro/documento"
                              }
                          } else {
                            if (bodyEscritura.lenght >0) {
                              res.status(200);
                              response = bodyEscritura[0];  // Devolvemos el usuario que hemos encontrado
                            }else{
                              res.status(200);
                            }
                          }
                          res.send(response);     // Hacemos el res.send al final de la función manejadora del post
                        }
                      );    // Fin del post de creación de nuevo usuario

                    } // fin de la función manejadora de la búsqueda de max id
                  ); // fin del get del max id


                } // fin de else como está ese email, damos de alta
              } // Fin de no hay error al buscar email
            } // Fin función manejadora de buscar email ya en la bbdd
          ) ;    // Fin del get

  }

);    // Fin del V2 register


// VERSION 2 de newaccount (dar de alta) una cuenta de un usuario,
// buscando si ya estuviera en MongoDB, lo que obtenemos
// mediante la API a MLAB
// Hacemos un post, no un put. Post es para crear y Put para modificar
app.post("/apiecs/v2/newaccount",
  function(req,res) {
              console.log("POST  /apiecs/v2/newaccount");


          var account = {
            "IBAN" : req.body.IBAN,
            "balance" : 0.0 + req.body.balance,
            "user_id" : req.body.user_id,
            "id" : 0      // En principioa cero, ya lo rellenaremos después
                      // con el máximo que haya en la bbdd + 1
          }

          // Vamos a comprobar si el el cliente que ha invocado a este método pasa
          // un webtoken correcto. Si no, no devuelvo datos.
          var token = req.headers['authorization']

           if(!token){
               res.status(401).send({
                 error: "Es necesario el token de autenticación"
               })
               return
           }

           token = token.replace('Bearer ', '')

           jwt.verify(token, 'Password secreta', function(err, user) {
             if (err) {
               res.status(401).send({
                     error: 'Token inválido'
                   });
             } else {
                  console.log("Token válido");
             }
           });

           // Si llegamos aquí es porque se ha verificado el token correctamente.
           // Si no, ya se habría hecho un send del error.

          // esto es lo que se usa para hacer una query en la API de Mlab,
          // para ver si el IBAN  ya existía. SÓLO se podrá tener un mismo IBAN
          // por persona, es decir, no se podrán compartir cuentas.
          var query = 'q={"IBAN":"' + account.IBAN  +'"}'

          var httpClient = requestJson.createClient(baseMLabURL);

          var response = {};

          // En esta variable meteremos el valor máximo del id encontrado,
          // para insertar ese máximo + 1 en el nuevo usuario que se cree
          var maxid = 0;

          // A partir de aqui se hacen las peticion para buscar la cuenta,
          // incluyendo el substring de la query y el &:
          httpClient.get("account?" + query + "&" + mLabAPIKey,
            // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
            // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
            function(err, resMLab, body){

              if (err){
                 response =  {
                  "msg" : "Error buscando IBAN ya existente"
                }
                res.status(500);
                res.send(response);
              } else {
                if (body.length > 0){
                  response = {
                    "msg" : "Ya existe ese IBAN  en la base de datos"
                  };
                  res.status(500);
                  res.send(response);

                } else {     // El body de la petición de get es vacío,
                            //no se ha encontrado el IBAN, entonces, damos de
                            //alta uno

                      // Primero vamos a buscar el valor máximo del id, para
                      // crear el nuevo con el número siguiente.
                      // se hace una query ern la que sólo se ordena
                      // por id descendente y se coge un único registro

                      // sólo traemos el id, ordenamos descendente y cogemos
                      // el primer registro, así sacamos el máximo
                      var querymaxid = 'f={"id": 1}&s={"id":-1}&l=1';

                      var responsemaxid = {};

                      // A partir de aqui se hacen las peticion para buscar
                      // el máximo del id,
                      // incluyendo el substring de la query y el &:
                      httpClient.get("account?" + querymaxid + "&" + mLabAPIKey,
                        // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
                        // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
                        function(errmaxid, resMLabmaxid, bodymaxid){

                          if (errmaxid){
                             responsemaxid =  {
                              "msg" : "Error buscando el máximo id"
                            }
                            res.status(500);
                            res.send(responsemaxid);
                            console.log("Error al buscar el máximo id");
                            console.log (err);
                          } else {
                            if (bodymaxid.length > 0){
                              maxid = bodymaxid[0].id ;

                            } else {     // El body de la busqueda de max id es vacío,
                              //no se ha encontrado ningún registro, entonces
                              // lo ponemos a 0, para que se meta el nuevo id =1
                                  maxid = 0;
                            }
                          }

                        // Ahora que ya tenemos el máximo id actual de la bbdd,
                        // vamos a rellenar el único campo que nos quedaba

                        account.id = maxid + 1;

                        httpClient.post("account?" + mLabAPIKey , account,
                        function (errEscritura,resMLabEscritura, bodyEscritura){
                          if (errEscritura){
                            res.status(505);
                            response =  {
                             "msg" : "Error escribiendo el nuevo registro/documento"
                              }
                          } else {
                            if (bodyEscritura.lenght >0) {
                              res.status(200);
                              response = bodyEscritura[0];  // Devolvemos el usuario que hemos encontrado
                            }else{
                              res.status(200);
                            }
                          }
                          res.send(response);     // Hacemos el res.send al final de la función manejadora del post
                        }
                      );    // Fin del post de creación de nuevo usuario

                    } // fin de la función manejadora de la búsqueda de max id
                  ); // fin del get del max id


                } // fin de else como está ese email, damos de alta
              } // Fin de no hay error al buscar email
            } // Fin función manejadora de buscar email ya en la bbdd
          ) ;    // Fin del get

  }

);    // Fin del V2 newaccount



// Query para obtener los movimientos de una cuenta pasada en la cabecera
// Y de unas fechas desde y hasta, si es que vienen
app.get("/apiecs/v2/users/account/movements",
  function (req,res){
    console.log("GET /apiecs/v2/users/account/movements");

    // recojo el id que es un parámetro
    var id = req.headers.id_cuenta;
    // recojo fechas desde hasta, si vienen
    var fecha_desde = req.headers.fecha_desde;
    var fecha_hasta = req.headers.fecha_hasta;



    // Vamos a comprobar si el el cliente que ha invocado a este método pasa
    // un webtoken correcto. Si no, no devuelvo datos.
    var token = req.headers['authorization']

     if(!token){
         res.status(401).send({
           error: "Es necesario el token de autenticación"
         })
         return
     }

     token = token.replace('Bearer ', '')

     jwt.verify(token, 'Password secreta', function(err, user) {
       if (err) {
         res.status(401).send({
               error: 'Token inválido'
             });
       } else {
            console.log("Token válido");
       }
     });

     // Si llegamos aquí es porque se ha verificado el token correctamente.
     // Si no, ya se habría hecho un send del error.


    var query = 'q={"account_id":' + id ;

    if( fecha_desde !== undefined){
        query = query + ',"date" : {"$gte" : {"$date":"' + fecha_desde + '"}';
    }
    if( fecha_hasta !== undefined){
      if( fecha_desde !== undefined){
        query = query + ', "$lte" : {"$date":"'+ fecha_hasta + '"}}';
      } else {
          query = query + ',"date" : {"$lte" : {"$date":"' + fecha_hasta + '"}}' ;
      }
    }
    query = query + '}';

    var httpClient = requestJson.createClient(baseMLabURL);

    // A partir de aqui se hacen las peticiones para la query, buscamos en cuentas, incluyendo el substring de la query y el &:
    httpClient.get("movement?" + query + "&" + mLabAPIKey,
      // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
      // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
      function(err, resMLab, body){

        var response = {};

        if (err){
           response =  {
            "msg" : "Error obteniendo movimientos de cuenta"
          }
          res.status(500);
        } else {
          if (body.length > 0){
            response = body;
          } else {
             response = {
               "msg" : "Movimiento no encontrado"
             };
             res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
);      // Fin del get de cuentas de un usuario /apiecs/v2/users/account/movements



// VERSION 2 de newmovement (dar de alta) un movimiento de una cuenta de un usuario,
// buscando si ya estuviera en MongoDB, lo que obtenemos
// mediante la API a MLAB
// Además se actualizará el balance de la cuenta sobre la que se está creando
// el movimiento
// Hacemos un post, no un put. Post es para crear y Put para modificar
app.post("/apiecs/v2/newmovement",
  function(req,res) {
              console.log("POST  /apiecs/v2/newmovement");


          var movement = {
            "account_id" : req.body.account_id,
            "date" : req.body.date,
            "amount" : req.body.amount,
            "type" : req.body.type,
            "description" : req.body.description,
            "id" : 0      // En principio a cero, ya lo rellenaremos después
                      // con el máximo que haya en la bbdd + 1
          }

          // Para actualizar el saldo, si se graba correctamente el movimiento
          var account = {
            "id" : req.body.account_id
          }

          // Vamos a comprobar si el el cliente que ha invocado a este método pasa
          // un webtoken correcto. Si no, no devuelvo datos.
          var token = req.headers['authorization']

           if(!token){
               res.status(401).send({
                 error: "Es necesario el token de autenticación"
               })
               return
           }

           token = token.replace('Bearer ', '')

           jwt.verify(token, 'Password secreta', function(err, user) {
             if (err) {
               res.status(401).send({
                     error: 'Token inválido'
                   });
             } else {
                  console.log("Token válido");
             }
           });

           // Si llegamos aquí es porque se ha verificado el token correctamente.
           // Si no, ya se habría hecho un send del error.

          var httpClient = requestJson.createClient(baseMLabURL);

          var response = {};

          // En esta variable meteremos el valor máximo del id encontrado,
          // para insertar ese máximo + 1 en el nuevo usuario que se cree
          var maxid = 0;


          // Primero vamos a buscar el valor máximo del id, para
          // crear el nuevo con el número siguiente.
          // se hace una query ern la que sólo se ordena
          // por id descendente y se coge un único registro

          // sólo traemos el id, ordenamos descendente y cogemos
          // el primer registro, así sacamos el máximo
          var querymaxid = 'f={"id": 1}&s={"id":-1}&l=1';

          var responsemaxid = {};

          // A partir de aqui se hacen las peticion para buscar
          // el máximo del id,
          // incluyendo el substring de la query y el &:
          httpClient.get("movement?" + querymaxid + "&" + mLabAPIKey,
            // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
            // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
            function(errmaxid, resMLabmaxid, bodymaxid){

              if (errmaxid){
                 responsemaxid =  {
                  "msg" : "Error buscando el máximo id"
                }
                res.status(500);
                res.send(responsemaxid);
                console.log("Error al buscar el máximo id");
                console.log (err);
              } else {
                if (bodymaxid.length > 0){
                  maxid = bodymaxid[0].id ;

                } else {     // El body de la busqueda de max id es vacío,
                  //no se ha encontrado ningún registro, entonces
                  // lo ponemos a 0, para que se meta el nuevo id =1
                      maxid = 0;
                }
              }

            // Ahora que ya tenemos el máximo id actual de la bbdd,
            // vamos a rellenar el único campo que nos quedaba

            movement.id = maxid + 1;

            httpClient.post("movement?" + mLabAPIKey , movement,
            function (errEscritura,resMLabEscritura, bodyEscritura){
              if (errEscritura){
                res.status(505);
                response =  {
                 "msg" : "Error escribiendo el nuevo registro/documento"
                  }
              } else {
                if (bodyEscritura.lenght >0) {
                  res.status(200);
                  response = {"id": movement.id};  // Devolvemos el id del movimiento creado
                }else{
                  res.status(200);
                  response = {"id": movement.id};  // Devolvemos el id del movimiento creado
              }

                // Una vez que hemos insertado correctamente el nuevo movimiento, vamos a actualizar
                // el balance de la Cuenta. Se incrementa el valor que hubiera
                // (se decrementará si es negativo, claro)
                var putBody = '{"$inc":{"balance":' + movement.amount + '}}';
                // esto es lo que se usa para hacer una query en la API de Mlab
                var queryupdateaccount = 'q={"id":' + account.id + '}'

                var httpClient = requestJson.createClient(baseMLabURL);


                        //Vamos a escribir los campos modificados en la base de datos
                        httpClient.put("account?" + queryupdateaccount + "&" + mLabAPIKey , JSON.parse(putBody),
                          function (errEscritura,resMLabEscritura, bodyEscritura){
                            if (errEscritura){
                              res.status(505);
                              response =  {
                               "msg" : "Error actualizando el saldo de la cuenta"
                                }
                            } else {
                              if (bodyEscritura.lenght >0) {
                                  res.status(200);
                                    // No devolvemos nada en el response, pues ya tenemos el id del movimiento que se creó
                              }else{
                                res.status(200);
                                // No devolvemos nada en el response, pues ya tenemos el id del movimiento que se creó
                              }
                            }
                          }
                        );    // Fin del put de actualizar saldo



              }  // Fin de else no hay error al escribir el movimiento
              res.send(response);     // Hacemos el res.send al final de la función manejadora del post
            }
          );    // Fin del post de creación de nuevo usuario

        } // fin de la función manejadora de la búsqueda de max id
      ); // fin del get del max id

  }

);    // Fin del V2 newmovement


// Query para obtener los datos de una cuenta, pasando el IBAN en el headers
app.get("/apiecs/v2/account",
  function (req,res){
    console.log("GET /apiecs/v2/account");

    // recojo el id que esté en el body
    var iban = req.headers.iban;

    // Vamos a comprobar si el el cliente que ha invocado a este método pasa
    // un webtoken correcto. Si no, no devuelvo datos.
    var token = req.headers['authorization']

     if(!token){
         res.status(401).send({
           error: "Es necesario el token de autenticación"
         })
         return
     }

     token = token.replace('Bearer ', '')

     jwt.verify(token, 'Password secreta', function(err, user) {
       if (err) {
         res.status(401).send({
               error: 'Token inválido'
             });
       } else {
            console.log("Token válido");
       }
     });

     // Si llegamos aquí es porque se ha verificado el token correctamente.
     // Si no, ya se habría hecho un send del error.


    var query = 'q={"IBAN":"' + iban + '"}'

    var httpClient = requestJson.createClient(baseMLabURL);

    // A partir de aqui se hacen las peticiones para la query, incluyendo el substring de la query y el &:
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
      // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
      function(err, resMLab, body){

        var response = {};
        if (err){
           response =  {
            "msg" : "Error obteniendo cuenta"
          }
          res.status(500);
        } else {
          if (body.length > 0){
            response = body;
          } else {
             response = {
               "msg" : "Cuenta no encontrada"
             };
             res.status(404);
          }
        }

        res.send(response);
      }
    )
  }
);   // fin get   /apiecs/v2/account


// Query para borrar el  movimientos con id pasado en la cabecera
app.put("/apiecs/v2/users/account/deletemovement",
  function (req,res){
    console.log("PUT /apiecs/v2/users/account/deletemovement");

    // recojo el id del movimiento, id de la cuenta y amount del headers
    var id = req.headers.id;
    var cuenta_id = req.headers.cuenta_id;
    var amount = req.headers.amount;

    // Vamos a comprobar si el el cliente que ha invocado a este método pasa
    // un webtoken correcto. Si no, no devuelvo datos.
    var token = req.headers['authorization']

     if(!token){
         res.status(401).send({
           error: "Es necesario el token de autenticación"
         })
         return
     }

     token = token.replace('Bearer ', '')

     jwt.verify(token, 'Password secreta', function(err, user) {
       if (err) {
         res.status(401).send({
               error: 'Token inválido'
             });
       } else {
            console.log("Token válido");
       }
     });

     // Si llegamos aquí es porque se ha verificado el token correctamente.
     // Si no, ya se habría hecho un send del error.



    var query = 'q={"id":' + id + '}';

    var httpClient = requestJson.createClient(baseMLabURL);

    // A partir de aqui se hacen las peticiones para la query, buscamos ese id para borrarlo
    // el body se pone vacío {}, para que borre el registro
    httpClient.put("movement?" + query + "&" + mLabAPIKey, {},
      // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
      // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
      function(err, resMLab, body){

        if (err){
           response =  {
            "msg" : "Error borrando movimiento de cuenta"
          }
          res.status(500);
        } else {

            response = body;

            // Una vez que hemos borrado el  movimiento, vamos a actualizar
            // el balance de la Cuenta. Se decrementa el valor que hubiera
            // (se incrementa si es negativo, claro)
            var putBody = '{"$inc":{"balance":' + -(amount) + '}}';
            // esto es lo que se usa para hacer una query en la API de Mlab
            var queryupdateaccount = 'q={"id":' + cuenta_id + '}'

                    //Vamos a escribir los campos modificados en la base de datos
                    httpClient.put("account?" + queryupdateaccount + "&" + mLabAPIKey , JSON.parse(putBody),
                      function (errEscritura,resMLabEscritura, bodyEscritura){
                        if (errEscritura){
                          res.status(505);
                          response =  {
                           "msg" : "Error actualizando el saldo de la cuenta"
                            }
                        } else {
                          if (bodyEscritura.lenght >0) {
                            res.status(200);
                                // No devolvemos nada en el response, pues ya tenemos el id del movimiento que se creó
                          }else{
                            res.status(200);
                            // No devolvemos nada en el response, pues ya tenemos el id del movimiento que se creó
                          }
                        }
                      }
                    );    // Fin del put de actualizar saldo de la cuenta

        }  // fin de si no hay error borrando movimiento
        res.send(response);
      }  // Fin de la función tratamiento put movement
    );   // Fin del put movement (el borrado de un movimiento)
  }   // Fin de la función  put /apiecs/v2/users/account/deletemovement
);      // Fin del put de cuentas de un usuario /apiecs/v2/users/account/deletemovement



// Servicio para envío de correo gmail cuando se ha olvidado la password
app.put("/apiecs/v2/mail",
  function (req,res){

      // Vamos a recuperar la información de ese usuario en bbdd
      console.log("PUT /apiecs/v2/mail");

      // Usamos el módulo para envío de correos
      const nodemailer = require('nodemailer');


      var email_usuario = req.headers.email;

      // esto es lo que se usa para hacer una query en la API de Mlab
      var query = 'q={"email":"' + email_usuario + '"}'

      var httpClient = requestJson.createClient(baseMLabURL);

      // A partir de aqui se hacen las peticiones para la query, incluyendo el substring de la query y el &:
      httpClient.get("user?" + query + "&" + mLabAPIKey,
        // tiene una función manejadora con los parámetros: error, response y body, donde estará el json devuelto
        // error 400 y pico => error en el cliente   y 500 y pico => error en el servidor
        function(err, resMLab, body){

          var response = {};

          if (err){
             response =  {
              "msg" : "Error obteniendo usuario"
            }
            res.status(500);
            res.send(response);

          } else {
            if (body.length > 0){
              // Hemos encontrado el usuario, entonces vamos a enviar correos
              // Ahora vamos a hacer el envío del email con la clave
              let transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                  type: 'OAuth2',
                  user: global.config.google.email,    // el remitente, siempre echocharlysierra
                  clientId: global.config.google.clientId,
                  clientSecret: global.config.google.apiKey,
                  refreshToken: global.config.google.refreshToken
                }
              });

              let mailOptions = {
                from: global.config.google.email,
                to: email_usuario,
                subject: 'The Count Accounts te recuerda',
                html: '<h1> ' + body[0].password +' </h1>'
              }



              transporter.sendMail(mailOptions, (err, info) => {
                if (err) throw new Error(err)

                res.statusCode = 200;
                response = {
                  "msg" : "Email enviado"
                };
                res.send(response);
              });
            } else {
               response = {
                 "msg" : "Usuario no encontrado"
               };
               res.status(404);
               res.send(response);
            }
          }
        }
      );    // Fin del get user
  }

);   //fin de put mail
